*Project superseded by [backdown](https://codeberg.org/vifon/backdown)
which reuses most of the scripts, but manages the devices in
a different way.  It's still just as functional as it was, but no
longer actively developed.*

-------------------------------------------------------------------------------

A set of personal scripts to create and update sparse backups of my
most important directories.  Meant to be deployed to a pendrive or
a similar device and run from there.

Not intended to be generic, configurable or even clean.
Instead intended to stay relatively simple and easy to modify with no
overengineered abstractions getting in the way.  Preferably pure rsync
with heavy use of the rsync filters to declare the files and
directories intended for backup.

See: `man rsync | less -p '^FILTER RULES'`


# Usage

## Installation / script management

The scripts `deploy.sh` and `fetch.sh` are used to manage the actual
backup scripts, with either their postional argument or the
environmental variable `$REMOTE` controlling their target locations.

`deploy.sh` copies the scripts onto a device or updates the
existing ones.

`fetch.sh` copied the scripts on a device back to this repository.
It's needed because I prefer to develop my backup scripts directly on
the backup device to avoid running `deploy.sh` constantly
during development.

Both scripts by default do merely a dry-run.  For them to actually
copy the files, pass the `-f` flag.

Both scripts accept the `-l` flag to also push/pull the per-device
local files in the `local/` directory.  Similarly, without `-f` it's
only a dry-run.

# Design

## Goals

* easy to deploy to a new device
* fully automated backup governed by rules set beforehand, with no
  doubts or decisions to make on each run

## Conventions

The `files/` directory reflects the state of target device to contain
the backup.

Each directory directly under `files/` contains a script called
`pull.sh` (the full path is `files/*/pull.sh`) that will sync the
backup against the live system.

Each such script should respect the variable `$RSYNC_FLAGS` that can
be used to add custom `rsync` flags to the usual execution.

`files/pull.sh` is a script running `pull.sh` inside each directory.

Each `pull.sh` should contain little more than a call to `rsync` to
copy over the files intended for this directory, possibly a few such
calls.

Any filtering logic should be performed with the `--filter` option and
merging the file called `rsync-filter.txt` and possibly additional
`rsync-*-filter.txt` files loaded from it.  If a filter
`rsync-local-filter.txt` exists, it is considered local to a device
and is not a part of this repository.

## Important decisions

* Files like `.gitignore` are *not* respected.  If a file exists in
  a repository worktree, it should exist in a backup.  Any exclusions
  should be done purely for storage saving purposes (think:
  `node_modules/` etc).
* The whole `files/` directory itself could just as well be a Git
  repository, then it would be pushed onto a device with `git push`
  instead of using the `deploy.sh` script.  It would simplify some
  things, but would also make some other matters harder and/or
  inconsistent, for instance handling of the `local/` directory.
  Then there is the simple fact of the target device then having a Git
  repository at its top-level making any backed up repositories nested
  inside it, which can be confusing and can cause all sorts of
  silly mistakes.

## Roadmap

* Multidevice sync
  * A safe and consistent way to push the backups back to the running
    system.  Currently this is done by hand on a case-by-case basis.
  * A good workflow for backing up from multiple systems.
