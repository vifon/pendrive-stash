#!/usr/bin/env bash

# Copy the scripts onto the remote, possibly a fresh one.

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

set -o errexit -o nounset -o pipefail

cd "$SCRIPTDIR"

. ./common.sh

rsync -avhxX \
      $DRY \
      --itemize-changes \
      --filter="merge ./rsync-filter.txt" \
      --info=progress2 \
      files/ \
      "$REMOTE"

if [ -d "local/$(basename "$REMOTE")" ]; then
    if [ -n "$LOCAL" ]; then
        rsync -avhxX \
              $DRY \
              --itemize-changes \
              --filter="merge ./rsync-local-filter.txt" \
              --info=progress2 \
              "local/$(basename "$REMOTE")/" \
              "$REMOTE"
    fi
else
    rsync -avhxX \
          $DRY \
          --filter="merge ./rsync-local-filter.txt" \
          --info=progress2 \
          --ignore-existing \
          "local/template/" \
          "$REMOTE"
fi
