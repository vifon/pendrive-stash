DRY="--dry-run"
LOCAL=""
while getopts :fl OPT; do
    case $OPT in
        f|+f)
            DRY=""
            ;;
        l|+l)
            LOCAL=1
            ;;
        *)
            echo "usage: ${0##*/} [+-f] [--] ARGS..."
            exit 2
    esac
done
shift $(( OPTIND - 1 ))
OPTIND=1


if (( $# > 0 )); then
    REMOTE="$1"
    shift
fi

# $REMOTE should be set either by the caller, preferably with direnv,
# or with a positional argument.  This assignment enforces with
# "set -u" that $REMOTE is not unset.
REMOTE="$REMOTE"
