#!/usr/bin/env bash

# Fetch the scripts from the remote back to this repository.

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

set -o errexit -o nounset -o pipefail

cd "$SCRIPTDIR"

. ./common.sh

rsync -avhxX \
      $DRY \
      --delete --delete-excluded \
      --itemize-changes \
      --filter="merge ./rsync-filter.txt" \
      --info=progress2 \
      "$REMOTE/" \
      files

if [ -n "$LOCAL" ]; then
    rsync -avhxX \
          $DRY \
          --itemize-changes \
          --filter="merge ./rsync-local-filter.txt" \
          --info=progress2 \
          --mkpath \
          "$REMOTE/" \
          "local/$(basename "$REMOTE")/"
fi
