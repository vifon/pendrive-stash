#!/usr/bin/env bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

set -o errexit -o nounset -o pipefail

cd "$SCRIPTDIR"


RSYNC_FLAGS=${RSYNC_FLAGS:-}

rsync -avhxX \
      --delete --delete-excluded \
      --filter="merge ./rsync-filter.txt" \
      --info=progress2 \
      $RSYNC_FLAGS \
      "$HOME/sync.d/" \
      .
