#!/usr/bin/env bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

set -o errexit -o nounset -o pipefail

cd "$SCRIPTDIR"

case "$HOSTNAME" in
    bifrost)
        ;;
    *)
        printf 'Unknown machine "%s", aborting!\n' "$HOSTNAME" >&2
        exit 1
esac

set -x

env -C ./books ./pull.sh
env -C ./notes ./pull.sh
env -C ./syncthing ./pull.sh
env -C ./movies ./pull.sh -f
env -C ./home ./pull.sh -f
