#!/usr/bin/env bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

set -o errexit -o nounset -o pipefail

cd "$SCRIPTDIR"


RSYNC_FLAGS=${RSYNC_FLAGS:-}

rsync -avhxX --delete --delete-excluded \
      --info=progress2 \
      -FF \
      $RSYNC_FLAGS \
      ~/work.d/ \
      work.d

rsync -avhxX --delete --delete-excluded \
      --info=progress2 \
      --filter "merge ./rsync-filter.txt" \
      -FF \
      $RSYNC_FLAGS \
      "$HOME/" \
      home

rsync -avhxX --delete --delete-excluded \
      --info=progress2 \
      -FF \
      $RSYNC_FLAGS \
      /media/Storage/documents/ \
      documents
