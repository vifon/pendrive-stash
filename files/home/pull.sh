#!/usr/bin/env bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

set -o errexit -o nounset -o pipefail

cd "$SCRIPTDIR"

DRY="--dry-run"
while getopts :f OPT; do
    case $OPT in
        f|+f)
            DRY=""
            ;;
        *)
            echo "usage: ${0##*/} [+-f] [--] ARGS..."
            exit 2
    esac
done
shift $(( OPTIND - 1 ))
OPTIND=1


RSYNC_FLAGS=${RSYNC_FLAGS:-}

rsync -avhxX \
      $DRY \
      --delete --delete-excluded \
      --filter="merge ./rsync-filter.txt" \
      --info=progress2 \
      $RSYNC_FLAGS \
      "$HOME/" \
      "$HOME/.var" \
      .
