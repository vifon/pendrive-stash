# :- .gitignore
- node_modules/
- .venv/
- venv/
- .env/

+ /.snippets
+ /.bin

+ /projects

+ /configs
- /configs/emacs-config/emacs.d/straight/build-*
- /configs/emacs-config/emacs.d/straight/repos/
+ /configs/emacs-config/emacs.d/var/mc-list.el
- /configs/emacs-config/emacs.d/var/*

+ /Documents

+ /.config
: /.config/.rsync-filter

+ /.local
+ /.local/share
: /.local/share/.rsync-filter
- /.local/*

+ /.ssh
+ /.ssh/config
+ /.ssh/config.d
- /.ssh/*

. ./rsync-steam-filter.txt

protect /pull.sh
protect /rsync-filter.txt
protect /rsync-steam-filter.txt

- /*
