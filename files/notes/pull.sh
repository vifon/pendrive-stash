#!/usr/bin/env bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

set -o errexit -o nounset -o pipefail


RSYNC_FLAGS=${RSYNC_FLAGS:-}

rsync -avhxX --delete --delete-excluded --exclude=/.git/ \
      --info=progress2 \
      $RSYNC_FLAGS \
      ~/wiki/ "$SCRIPTDIR"/wiki

rsync -avhxXL -FF --delete --delete-excluded \
      --info=progress2 \
      $RSYNC_FLAGS \
      ~/.deft/ "$SCRIPTDIR"/deft

rsync -avhxXL --delete --delete-excluded \
      --info=progress2 \
      $RSYNC_FLAGS \
      ~/org/ "$SCRIPTDIR"/org
