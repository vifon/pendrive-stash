#!/usr/bin/env bash

SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" > /dev/null 2>&1 && pwd)"

set -o errexit -o nounset -o pipefail


RSYNC_FLAGS=${RSYNC_FLAGS:-}

rsync -avhxX --delete \
      --info=progress2 \
      $RSYNC_FLAGS \
      "/media/Multimedia/Calibre Library/" \
      "$SCRIPTDIR/Calibre Library"

rsync -avhxX --delete \
      --info=progress2 \
      $RSYNC_FLAGS \
      "/media/Multimedia/books/" \
      "$SCRIPTDIR/misc"

rsync -avhxX --delete -FF -L \
      --info=progress2 \
      $RSYNC_FLAGS \
      "/media/Storage/annex/books" \
      "/media/Storage/annex/docs/manuals" \
      "/media/Storage/annex/docs/technical" \
      "$SCRIPTDIR/annex"
